<?php

//Start the session
//session_start();

//include database connection
include 'database.php';


if( isset($_POST['name']) && isset($_POST['email']) && isset($_POST['password']) ) {
    try{
          
        //insert query
        // $query = "SELECT name FROM users.users WHERE email =:email AND password =:password";

        $query =  "INSERT INTO booust_project.users ( name, email, password, created_at ) VALUES ( :name, :email, :password, :created_at)";
        $stmt = $register-> prepare($query);


        
        //posted values
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $password = hash('md5', $password);
        $date = date('Y-m-d H:i:s');
        $id= 1;
        //bind the parameters
        // $stmt->bindParam(':id',$id);
        $stmt->bindParam(':name',$name);
        $stmt->bindParam(':email',$email);
        $stmt->bindParam(':password',$password);
        $stmt->bindParam(':created_at',$date);
        
        //Execute the query
        if($stmt->execute()) {
            $_SESSION['name'] = $name;
            header("Location: reg.php");
            exit();

        }else {
            echo "<div class='alert alert-danger'>Unable to save record.</div>";
        }
    
    }
    // show error
    catch(PDOException $exception) {
    die('ERROR: '. $exception->getMessage());
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Register</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
    * {
        box-sizing: border-box;
    }
    input[type=text] {
        width:100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }
    input[type=email] {
        width:100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }
    input[type=password] {
        width:100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }
    label {
        padding: 12px 12px 12px 0;
        display:inline-block;
    }
    input[type=submit] {
        background-color: #4CAF50;
        color: white;
        padding: 12px 20px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }
   
    input[type=submit]:hover {
    background-color: #45a049;
}

.container {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}

.col-25 {
    float: left;
    width: 25%;
    margin-top: 6px;
}

.col-75 {
    float: left;
    width: 75%;
    margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
    .col-25, .col-75, input[type=submit] {
        width: 100%;
        margin-top: 0;
    }
}
    </style>
</head>

<body>

<div class="container" align="center">
<h1>Register</h1>
</div>

<div class="container">
  <form action="register.php" method="POST">
  <div class="row">
      <div class="col-25">
        <label for="NAME">NAME</label>
        
      </div>
      <div class="col-75">
      <input type="text" id="name" name="name" placeholder="Enter Name.." required>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="email">EMAIL</label>
        
      </div>
      <div class="col-75">
      <input type="email" id="email" name="email" placeholder="Your email.." required>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="password">PASSWORD</label>
      </div>
      <div class="col-75">
        <input type="password" id="password" name="password">
      </div>
    </div>
    <div class="row">
      <input type="submit" value="Submit">
    </div>
    </form>
    <br>
    <p>I have an account. <a href="login.php">Login</a></p>
<div>

<body>
</html> 