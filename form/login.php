<?php

// this line of code starts a new session on page load
session_start();

//include db connection class
include 'register.php';
if (isset($_POST['email']) && isset($_POST['password'])){
 //  try{
             //this is the minimum sql query prepared statement for login
       $query = "SELECT name FROM booust_project.users WHERE email =email AND password =password";
       $stmt = $register->prepare($query);
       
       
       //Now i bind the parameters found to the parameters entered by the user
       $email = $_POST['email'];
       $password = hash('md5', $_POST['password']);
       
       $stmt->bindParam(':email', $email);
       $stmt->bindParam(':password', $password);
       $stmt->execute();
       
       //now to get the numbers of rows returned
       $num = $stmt->rowCount();
       
       if ( $num > 0 ){
           $row = $stmt->fetch();
           $_SESSION['name'] = $row['name'];
           header("Location:reg.php");
           exit();
       }
        else {
                       echo "<div class = 'alert alert-danger'>Username/ Password doesn't match! </div>";
                   }
                }
//catch (PDOException $exception){
  //  die('ERROR: '. $exception->getMessage());
//}
//}

else {
   
   session_destroy();
   
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Login Page</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    input[type=email] {
        width:100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }
    input[type=password] {
        width:100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }
    input[type=submit] {
        background-color: #4CAF50;
        color: white;
        padding: 12px 20px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }
   
    input[type=submit]:hover {
    background-color: #45a049;
    }
    </style>
</head>
<body>

<div class="container" align="center">
    <h1>Log In</div>
</div>

<div class="container">

    <form action="login.php" method="POST">

        <label for="email">EMAIL</label>
        <input type="email" id="email" name="email" required>

        <label for="password">PASSWORD</label>
        <input type="password" id="password" name="password" required>
        <br>
        <input type="submit" value="Submit">
        <br>
    </form>
    <br>
    <p>Don't have an account? <a href="register.php">Register</a></p>

    </div>

    </body>
    </html>

