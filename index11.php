<!DOCTYPE html>
<html>
<head>
	<title>Home page</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet" media="all">
      <link href="css/index.css" type="text/css" rel="stylesheet" media="all">
      <link rel="icon" href="images/fav.jpg">
	</head>
	<style>
	body{
		background:url(images/3.jpg) no-repeat center center fixed;
		-webkit-background-size:cover;
		-moz-background-size:cover;
		-o-background-size:cover;
		background-size:cover;
		text-align:center;
		height: 100%;
	}
		</style>
	
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="#"><img src="images/logo1.jpg" class="logo"><strong>Adden</strong></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExample05">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index11.html">Gallery</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="moviegallery.html">Youtube Trailers</a>
          </li>
         <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Movies</a>
            <div class="dropdown-menu" aria-labelledby="dropdown05">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Sci-Fi</a>
              <a class="dropdown-item" href="#">Fiction</a>
            </div>
          </li>
        </ul>
          <ul class="nav navbar-nav navbar-right ml-auto mr-auto">
					<li class="nav-item"><a class="nav-link " href="signin.html">Sign In</a></li>
					<li class="nav-item"><a class="nav-link " href="#">Sign Up</a></li>
          </ul>
        <form class="form-inline my-2 my-md-0">
          <input class="form-control" type="text" placeholder="Search">&nbsp;
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
      </div>
    </nav>
    
	<div class="container-fluid">
	<hr class="my-4">
	<div class="jumbotron jumbotron-fluid text-center text-lg">
 <h1 class="text-danger !important"> MOVIE RENTAL AT ITS BEST</h1>
	<p> AddenFlix is an online movie rental store where viewer can buy, 
	watch online and download any movie of their choice. 
	We are ready to satisfy our customers and make ready the 
	latest movies in Hollywood, Nollywood, Tollywood and the likes.</p> 
</div>
<div class="well text-center">We Look forward to your Patronage</div>
	
<br>
<h2 style="font-family:arial helvetica;"> 2018 Movies</h2>
<div class="row row-padding">
  <div class="column center">
    <img src="images/Aquaman.jpg" alt="Aquaman" style="width:100%">
	<figcaption>
	<b>Aquaman</b>
	</figcaption>
  </div>
  <div class="column">
    <img src="images/Avengers-Infinity-War.jpg" alt="Infinity Wars" style="width:100%">
	<figcaption>
	<b>Infinity-War</b>
	</figcaption>
  </div>
  <div class="column">
    <img src="images/Deadpool-2.jpg" alt="Deadpool-2" style="width:100%">
	<figcaption>
	<b>Deadpool-2</b>
	</figcaption>
  </div>
   <div class="column">
    <img src="images/Dark-Crimes.jpg" alt="Dark-Crimes" style="width:100%">
	<figcaption>
	<b>Dark Crimes</b>
	</figcaption>
  </div>
   <div class="column">
    <img src="images/Mission-Impossible-Fallout.jpg" alt="Mission-Impossible-Fallout" style="width:100%">
	<figcaption>
	<b>Mission-Impossible-Fallout</b>
	</figcaption>
  </div>
   <div class="column">
    <img src="images/Mowgli.jpg" alt="Mowgli" style="width:100%">
	<figcaption>
	<b>Mowgli</b>
	</figcaption>
  </div>
</div>
<br>
<div class="row row-padding">
  <div class="column">
    <img src="images/Black-Panther.jpg" alt="Black Panther" style="width:100%">
	<figcaption>
	<b>Black Panther</b>
      </figcaption>
  </div>
  <div class="column">
    <img src="images/Ready-Player-One.jpg" alt="Infinity Wars" style="width:100%">
	<figcaption>
	<b>Ready Player One</b>
	</figcaption>
  </div>
  <div class="column">
    <img src="images/Kickboxer-Retaliation.jpg" alt="Kickboxer-Retaliation" style="width:100%">
	<figcaption>
	<b>Kickboxer-Retaliation</b>
	</figcaption>
  </div>
    <div class="column">
    <img src="images/The-Predator.jpg" alt="The-Predator" style="width:100%">
	<figcaption>
	<b>The-Predator</b>
        </figcaption>
  </div>
  <div class="column">
    <img src="images/Robin-Hood-Origins.jpg" alt="Robin-Hood-Origins" style="width:100%">
	<figcaption>
	<b>Robin-Hood-Origins</b>
	</figcaption>
  </div>
  <div class="column">
    <img src="images/The-Maze-Runner-The-Death-Cure.jpg" alt="The-Maze-Runner-The-Death-Cure" style="width:100%">
	<figcaption>
	<b>The-Maze-Runner-The-Death-Cure</b>
	</figcaption>
  </div>
</div>

<br>
<h2 style="font-family:arial helvetica;"> 2017 Movies</h2>
<div class="row row-padding">
  <div class="column">
    <img src="images/Alien-Covenant.jpg" alt="Alien-Covenant" style="width:100%">
	<figcaption>
	<b>Alien-Covenant</b>
	</figcaption>
  </div>
  <div class="column">
    <img src="images/Baywatch.jpg" alt="Baywatch" style="width:100%">
	<figcaption>
	<b>Baywatch</b>
	</figcaption>
  </div>
  <div class="column">
    <img src="images/IT.jpg" alt="IT" style="width:100%">
	<figcaption>
	<b>IT</b>
	</figcaption>
  </div>
   <div class="column">
    <img src="images/power-rangers.jpg" alt="Power Rangers" style="width:100%">
	<figcaption>
	<b>Power Rangers</b>
	</figcaption>
  </div>
   <div class="column">
    <img src="images/Rings.jpg" alt="Rings" style="width:100%">
	<figcaption>
	<b>Rings</b>
	</figcaption>
  </div>
   <div class="column">
    <img src="images/Justice-League.jpg" alt="Justice League" style="width:100%">
	<figcaption>
	<b>Justice League</b>
	</figcaption>
  </div>
</div>
<br>
<div class="row row-padding">
  <div class="column">
    <img src="images/Table-19.jpg" alt="Table-19" style="width:100%">
	<figcaption>
	<b>Table-19</b>
      </figcaption>
  </div>
  <div class="column">
    <img src="images/The-Hitmans-Bodyguard.jpg" alt="The Hitmans Bodyguard" style="width:100%">
	<figcaption>
	<b>The Hitmans Bodyguard</b>
	</figcaption>
  </div>
  <div class="column">
    <img src="images/The-Blackcoats-Daughter.jpg" alt="The Blackcoats Daughter" style="width:100%">
	<figcaption>
	<b>The Blackcoats Daughter</b>
	</figcaption>
  </div>
    <div class="column">
    <img src="images/The-Disaster-Artist.jpg" alt="The Disaster Artist" style="width:100%">
	<figcaption>
	<b>The Disaster Artist</b>
        </figcaption>
  </div>
  <div class="column">
    <img src="images/The-Fate-Of-The-Furious.jpg" alt="The Fate Of The Furious" style="width:100%">
	<figcaption>
	<b>The Fate Of The Furious</b>
	</figcaption>
  </div>
  <div class="column">
    <img src="images/Thor-Ragnarok.jpg" alt="Thor Ragnarok" style="width:100%">
	<figcaption>
	<b>Thor Ragnarok</b>
	</figcaption>
  </div>
  
</div>
</div>

    <footer class="pt-4 my-md-5 pt-md-5 border-top bg-dark  footer-style">
        <div class="row">
          <div class="col-12 col-md">
            <img class="mb-2" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="24" height="24">
            <small class="d-block mb-3 text-muted text-large">&copy; Adden MovieFlix Inc. 2018</small>
          </div>
          <div class="col-6 col-md">
            <h5>Features</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Adden Movies Promo</a></li>
              <li><a class="text-muted" href="#">Adden Music Box</a></li>
              <li><a class="text-muted" href="#">Adden Group</a></li>
              <li><a class="text-muted" href="#">Adden Customer Satisfaction</a></li>
              <li><a class="text-muted" href="#">Adden Slides</a></li>
              <li><a class="text-muted" href="#">Contact Us</a></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Resources</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Hollywood News</a></li>
              <li><a class="text-muted" href="#">Trailers</a></li>
              <li><a class="text-muted" href="#">More</a></li>
              <li><a class="text-muted" href="#">Our Blog</a></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>About</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Adden Team</a></li>
              <li><a class="text-muted" href="#">Locations</a></li>
              <li><a class="text-muted" href="#">Privacy</a></li>
              <li><a class="text-muted" href="#">Terms</a></li>
            </ul>
          </div>
        </div>
    </footer>	
        		
</body>

</html>